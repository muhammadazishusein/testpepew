from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
def index(request):
    if request.method == 'GET':
        form = StatusForm()
    elif request.method == 'POST':
        form = StatusForm(request.POST)

        if form.is_valid():
            new_status = Status(
                content = form.data['content'],
            )

            new_status.save()
            form = StatusForm()

        return HttpResponseRedirect('/')
    
    status = Status.objects.all()

    context = {
        'form': form,
        'status': status,
    }

    return render(request, 'index.html', context)