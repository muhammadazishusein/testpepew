from django.db import models
import datetime

#Create your models here.
class Status(models.Model):
    content = models.TextField(default = '', max_length = 300)
    date = models.DateTimeField(auto_now = True)