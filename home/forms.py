from django import forms
from .models import Status
from django.forms import ModelForm, Textarea
import datetime

class StatusForm(ModelForm):
    class Meta:
        model = Status

        fields = ['content']

        widgets = {
            'content': Textarea(attrs={'cols': 80, 'rows': 5}),
        }